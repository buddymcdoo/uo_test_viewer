﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOTestViewer
{
    //This class holds the data for each individual test
    public class Test
    {
        int ID;
        int AssociatedClassID;
        bool IsAnswerKey;
        int Score;
        string Term;
        string Version;
        string Name;
        byte[] PDF;

        public Test(int id, int associatedClassId, bool isAnswerKey, int score, string term, string version, string name, byte[] pdf) {
            ID = id;
            AssociatedClassID = associatedClassId;
            IsAnswerKey = isAnswerKey;
            Score = score;
            Term = term;
            Version = version;
            Name = name;
            PDF = pdf;
        }

        public int getID() { return ID; }
        public int getAssociatedClassID() { return AssociatedClassID; }
        public bool getIsAnswerKey() { return IsAnswerKey; }
        public int getScore() { return Score; }
        public string getTerm() { return Term; }
        public string getName() { return Name; }
        public byte[] getPDF() { return PDF; } 
    }
}
