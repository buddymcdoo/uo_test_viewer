﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace UOTestViewer
{
    public partial class ConnectionSetter : Form
    {
        public ConnectionSetter()
        {
            CenterToScreen();
            Location = new Point(Location.X - (2 * Width / 3), Location.Y + (Height / 4));
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = textBox1.Text;

            try //tries to connect to database (by creating an instance of our CSharpSqlManager class) before anything
            {
                CSharpSqlManager sqlManager = new CSharpSqlManager(connectionString);

                string connectionTextFile = Path.Combine(Directory.GetCurrentDirectory(), "DatabaseConnectionInfo.txt");

                File.WriteAllText(connectionTextFile, connectionString);

                Close();
            }
            catch (Exception)//if fails, the program won't run
            {
                MessageBox.Show("The connection string you have entered doesn't work.\nPlease try another connection string.");
            }
        }
    }
}
