﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using iText.Layout;
using iText.Kernel.Pdf;
using iText.Layout.Element;
using PdfToImage;


namespace UOTestViewer
{
    public partial class PrimaryUserForm : Form
    {
        //MessageBox.Show(Directory)
        string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\simon\Source\Repos\uo_test_viewer\UOTestViewer\UOTestViewer\Database1.mdf;Integrated Security=True";
        CSharpSqlManager sqlManager;
        string[] usersTableColumnNames = { "Id", "UOID", "IsAdmin", "AssociatedClasses", "Password" };
        string[] classesTableColumnNames = { "Id", "Name" };
        string[] testsTableColumnNames = { "Id", "AssociatedClassID", "IsAnswerKey", "Score", "Term", "Version", "Name", "PDF", "Image" };
        object[] tempQuery = null;//This is for making queries with our CSharpSqlManager class

        string theUsername = "";
        string thePassword = "";
        bool personIsAdmin = false;
        List<string> theAssociatedClasses = new List<string>();
        List<string> theAssociatedClassesNames = new List<string>();
        List<Test> theTests = new List<Test>();
        List<Course> theClasses = new List<Course>();

        //(For viewing the tests)
        private int ImageWidth, ImageHeight;// The image's original size.
        private float ImageScale = 1.0f;// The current scale.
        private bool mouseDown;
        private Point lastLocation;

        public PrimaryUserForm()
        {
            string directoryMarker = Directory.GetCurrentDirectory();
            string databaseConnectionFile = Path.Combine(directoryMarker, "DatabaseConnectionInfo.txt");

            if (!File.Exists(databaseConnectionFile))
                File.Create(databaseConnectionFile);

            connectionString = File.ReadAllText(databaseConnectionFile);

            if (connectionString == "")
            {
                //https://stackoverflow.com/questions/8004036/how-to-set-application-shutdown-mode-in-a-c-sharp-windows-forms-project
                ConnectionSetter myConnectionSetter = new ConnectionSetter();
                Application.Run(myConnectionSetter);
            }

            connectionString = File.ReadAllText(databaseConnectionFile);

            InitializeComponent();

            try //tries to connect to database (by creating an instance of our CSharpSqlManager class) before anything
            {
                sqlManager = new CSharpSqlManager(connectionString);
                if (!sqlManager.TABLE_EXISTS("Tests"))
                {
                    sqlManager.CREATE_TABLE("Tests", "Id INT, AssociatedClassID INT, IsAnswerKey BIT, Score INT, Term NVARCHAR(MAX), Version NVARCHAR(MAX), Name NVARCHAR(MAX), PDF VARBINARY(MAX), Image VARBINARY(MAX), TestAnswers NVARCHAR(MAX), KeyAnswers NVARCHAR(MAX)");
                }
                if (!sqlManager.TABLE_EXISTS("Users"))
                {
                    sqlManager.CREATE_TABLE("Users", "Id INT, UOID NVARCHAR(MAX), IsAdmin BIT, AssociatedClasses NVARCHAR(MAX), Password NVARCHAR(MAX)");
                    //Creates the default user with username 123456789 and password 123456789
                    sqlManager.INSERT_("Users", new string[] { usersTableColumnNames[0], usersTableColumnNames[1], usersTableColumnNames[2], usersTableColumnNames[3], usersTableColumnNames[4] }, new object[] { 1, "123456789", true, "", "123456789" });
                }
                if (!sqlManager.TABLE_EXISTS("Classes"))
                {
                    sqlManager.CREATE_TABLE("Classes", "Id INT, Name NVARCHAR(MAX)");
                }
            }
            catch (Exception e)//if fails, the program won't run
            {
                this.Enabled = false;
                MessageBox.Show("Could not connect to the database.\nContact your administrator for help.");
            }



            //For zooming in and out of test pictures.
            ImageWidth = 484;
            ImageHeight = 611;
            pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox2.Size = new Size(ImageWidth, ImageHeight);
            MouseWheel += new MouseEventHandler(pictureBox2_MouseWheel);
        }

        ~PrimaryUserForm()
        {
            //Delete all temp graded tests
            string directoryGradedTests = Path.GetFullPath(Directory.GetCurrentDirectory() + "/Graded Tests/");//Directory of where we add PDFs of test grade sheets
            foreach(string file in Directory.GetFiles(directoryGradedTests))
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button8.Enabled = true;
            comboBox1.Items.Clear();
            foreach (string aClass in theAssociatedClassesNames)
                comboBox1.Items.Add(aClass);
            tabControl3.TabPages[0].Enabled = true;
            tabControl3.SelectedTab = tabControl3.TabPages[0];
            tabControl3.TabPages[1].Enabled = false;
            tabControl3.SelectedTab = tabControl3.TabPages[0];
            tabControl2.SelectedTab = tabControl2.TabPages[1];
            comboBox2.Items.Clear();
            comboBox2.Items.Add("Fall");
            comboBox2.Items.Add("Winter");
            comboBox2.Items.Add("Spring");
            comboBox2.Items.Add("Summer");
            textBox3.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            listBox6.Items.Clear();
            listBox7.Items.Clear();
            listBox8.Items.Clear();
            textBox4.Clear();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Please enter your username.\n(This should be the same as your 95 number).");
            }
            else if (textBox2.Text == "")
            {
                MessageBox.Show("Please enter your password.");
            }
            else
            {
                bool canLogin = false;
                string tempUsername = textBox1.Text;
                string tempPassword = textBox2.Text;

                //This is the first demonstration of our CSharpSQLManager in action!
                tempQuery = sqlManager.SELECT_("dbo.Users", new string[] { usersTableColumnNames[1], usersTableColumnNames[4] }, new object[] { tempUsername, tempPassword });
                if(tempQuery != null)
                {
                    theUsername = tempUsername;
                    thePassword = tempPassword;
                    personIsAdmin = (bool)tempQuery[2];
                    if ((string)tempQuery[3] != "")
                    {
                        string[] associatedClasses = ((string)tempQuery[3]).Split(',');
                        foreach (string classid in associatedClasses)
                        {
                            theAssociatedClasses.Add(classid.Trim());
                            tempQuery = sqlManager.SELECT_("dbo.Classes", new string[] { classesTableColumnNames[0] }, new object[] { Int32.Parse(classid.Trim()) });
                            if(tempQuery != null)
                                theAssociatedClassesNames.Add((string)tempQuery[1]);
                        }
                    }
                    canLogin = true;
                }
                
                //If username (which is our 95number) and password are accepted, login.
                if (canLogin)
                {
                    //Get user login data
                    //------------------------------------------------
                    for (int i = 0; i < theAssociatedClasses.Count; i++)
                    {
                        tempQuery = sqlManager.SELECT_("dbo.Tests", new string[] { testsTableColumnNames[1] }, new object[] { theAssociatedClasses[i] });
                        if (tempQuery != null)
                        {
                            int id = (int)tempQuery[0];
                            int associatedClassId = (int)tempQuery[1];
                            bool isAnswerKey = (bool)tempQuery[2];
                            int score = (int)tempQuery[3];
                            string term = (string)tempQuery[4];
                            string version = (string)tempQuery[5];
                            string name = (string)tempQuery[6];
                            byte[] pdf = (byte[])tempQuery[7];
                            theTests.Add(new Test(id, associatedClassId, isAnswerKey, score, term, version, name, pdf));
                        }

                        tempQuery = sqlManager.SELECT_("dbo.Classes", new string[] { classesTableColumnNames[0] }, new object[] { theAssociatedClasses[i] });
                        if (tempQuery != null)
                        {
                            int id = (int)tempQuery[0];
                            string name = (string)tempQuery[1];

                            theClasses.Add(new Course(id, name));
                        }
                    }
                    //------------------------------------------------

                    //Set up user info into the various tabs
                    //------------------------------------------------
                    foreach(Course aCourse in theClasses)
                    {
                        listBox1.Items.Add(aCourse.getName());
                    }
                    //------------------------------------------------
                    panel1.Visible = false;
                    panel2.Visible = false;
                    
                    panel3.Visible = false;
                    groupBox1.Visible = false;
                    listBox9.Items.Clear();
                    listBox10.Items.Clear();
                    listBox9.Items.Clear();
                    listBox10.Items.Clear();
                    pictureBox2.Visible = false;
                    tabControl2.SelectedTab = tabControl2.TabPages[0];
                    tabControl1.SelectedTab = tabControl1.TabPages[1];
                    tabControl2.Visible = true;
                    if (personIsAdmin == false)
                    {
                        button5.Enabled = false;
                        button5.Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show("The username or password you entered is incorrect.");
                }
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl2.SelectedTab = tabControl2.TabPages[0];
            pictureBox2.Visible = false;
            panel3.Visible = false;
            groupBox1.Visible = false;
            listBox9.Items.Clear();
            listBox10.Items.Clear();
            panel2.Visible = false;
            panel1.Visible = true;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                //Go through and find all tests associated with the class we've clicked on
                listBox2.Items.Clear();
                int selectedClassID = -1;

                tempQuery = sqlManager.SELECT_("dbo.Classes", new string[] { classesTableColumnNames[1] }, new object[] { listBox1.SelectedItem.ToString() });
                if (tempQuery != null)
                {
                    selectedClassID = (int)tempQuery[0];
                }
                //Add all tests that are associated with that class id
                if (selectedClassID != -1)
                {
                    tempQuery = sqlManager.SELECT_("dbo.Tests", new string[] { testsTableColumnNames[1] }, new object[] { selectedClassID.ToString() });
                    if (tempQuery != null)
                    {
                        listBox2.Items.Add(tempQuery[6].ToString());
                    }
                }
                panel3.Visible = false;
                groupBox1.Visible = false;
                listBox9.Items.Clear();
                listBox10.Items.Clear();
                panel2.Visible = true;
                panel1.Visible = false;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox3_MouseClick(object sender, MouseEventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox3.Text = openFileDialog1.FileName;
            }
        }

        private void listBox3_MouseClick(object sender, MouseEventArgs e)
        {
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                foreach (string file in openFileDialog2.FileNames)
                    listBox3.Items.Add(file);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
                MessageBox.Show("Please select a class.");
            else if (comboBox2.Text == "")
                MessageBox.Show("Please select a term.");
            else if (textBox4.Text == "")
                MessageBox.Show("Please enter a name for the test.");
            else if (textBox3.Text == "")
                MessageBox.Show("Please select a test key.");
            else if (!File.Exists(textBox3.Text))
                MessageBox.Show("The grading key you have selected\nis a non-existent file.");
            else if (listBox3.Items.Count <= 0)
                MessageBox.Show("Please choose at least one test for grading.");
            else
            {
                LoadingBar loadingBar = new LoadingBar();
                loadingBar.progressBar1.Minimum = 0;
                loadingBar.progressBar1.Maximum = 110 * 3;
                loadingBar.progressBar1.Value = 10;
                loadingBar.Show();
                bool foundNonExistentFile = false;
                foreach (var file in listBox3.Items)
                {
                    if (!File.Exists(file.ToString()))
                    {
                        MessageBox.Show("One or more files you have selected\nfor grading does not exist.");
                        foundNonExistentFile = true;
                        break;
                    }
                }
                if (!foundNonExistentFile)//If everything is in order, begin the grading process
                {
                    int size = listBox3.Items.Count;
                    string keyPath = textBox3.Text;
                    string[] filePaths = new string[size + 1];
                    filePaths[0] = keyPath;//<-----------------------This file (the first in listBox2) is the key file.

                    for (int i = 0; i <= size; i++)
                    {
                        try {
                            filePaths[i + 1] = listBox3.Items[i].ToString();
                        }
                        catch(Exception exc01)
                        {
                            //Move on
                        }
                    }

                    if (size < 1)
                        size = 1;
                    int updateAmount = 50 / size;

                    FileInfo tempFInfo = new FileInfo("pathMarker.txt");
                    string originalSaveFilePath = tempFInfo.DirectoryName + "\\Processed Sheet Data\\";
                    foreach (string file1 in filePaths)
                    {
                        //if (File.Exists(file1) == false)
                            //continue;
                        //Creates text files based off of scanned data and saves them to our directory (...\UserOutput\bin\Release\Processed Sheet Data\)
                        string imageData = "";
                        /* try
                            {*/
                        imageData = Program.scanImage(file1);
                        if(loadingBar.progressBar1.Value <= (loadingBar.progressBar1.Maximum - updateAmount))
                            loadingBar.progressBar1.Value += updateAmount;
                        //}
                        /* catch (Exception)
                            {
                                MessageBox.Show("ERROR: One or more files you have chosen was not properly analyzed!\n\t\tPlease try again.");
                                textBox1.Text = "";
                                loadingBar.Hide();
                                return;
                            }*/
                        string tempImageData = imageData;

                        string IDNumber = "";
                        try
                        {
                            IDNumber = tempImageData.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries)[0];//This gets the line in our string that has the UO ID Number.
                        }
                        catch (IndexOutOfRangeException)
                        {
                            //This means that there is nothing in the array
                        }

                        tempImageData = tempImageData.Replace("\n", "");//This is because we want comma-separated values).

                        string fileExtention = ".txt";
                        string withSpaces = IDNumber + "_" + theAssociatedClasses[comboBox1.SelectedIndex].ToString() + "_" + textBox4.Text;
                        string withoutSpacesOriginal = withSpaces.Replace(" ", "");
                        string withoutSpaces = withoutSpacesOriginal;
                        int counter = 1;
                        while (File.Exists(originalSaveFilePath + withoutSpaces + fileExtention))
                        {
                            withoutSpaces = withoutSpacesOriginal + "(" + (counter++).ToString() + ")";
                        }
                        withoutSpaces += fileExtention;
                        string saveFilePath = originalSaveFilePath + withoutSpaces;
                        System.IO.File.WriteAllText(saveFilePath, tempImageData);
                        if (loadingBar.progressBar1.Value <= (loadingBar.progressBar1.Maximum - updateAmount))
                            loadingBar.progressBar1.Value += updateAmount;
                    }
                    //Add graded tests to box
                    //MessageBox.Show(originalSaveFilePath);
                    DirectoryInfo dInfo = new DirectoryInfo(originalSaveFilePath);
                    System.IO.FileInfo[] Files = dInfo.GetFiles("*.txt");
                    foreach (System.IO.FileInfo file2 in Files)
                    {
                        listBox4.Items.Add(file2.Name);
                    }
                    //Create pdf for each graded test
                    try
                    {
                        if (listBox4.Items.Count > 0)
                        {
                            string theKey = listBox4.Items[0].ToString();
                            int locationCounter = 0;
                            foreach (string test in listBox4.Items)
                            {
                                string testAnswers = "";

                                testAnswers = File.ReadAllText(originalSaveFilePath + test);
                                string student95Number = new string(testAnswers.Where(c => char.IsDigit(c)).ToArray());
                                testAnswers = new string(testAnswers.Where(c => !char.IsDigit(c)).ToArray());
                                if (test == listBox4.Items[0].ToString())
                                    theKey = testAnswers;
                                else
                                {
                                    string connectedImage = listBox3.Items[locationCounter++].ToString();
                                    string[] tempStringArray = gradeTest(testAnswers, theKey, theAssociatedClasses[comboBox1.SelectedIndex].ToString(), textBox4.Text, student95Number, theAssociatedClasses[comboBox1.SelectedIndex].ToString());
                                    listBox5.Items.Add(tempStringArray[0]);//Pdf name of the graded test.
                                    listBox6.Items.Add(tempStringArray[1]);//Test answers of the graded test.
                                    listBox7.Items.Add(tempStringArray[2]);//Key answers of the graded test.
                                    listBox8.Items.Add(tempStringArray[3]);//Score of the graded test.
                                }
                            }
                        }
                        foreach(string myFile in Directory.GetFiles(originalSaveFilePath))
                        {
                            if (File.Exists(myFile))
                            {
                                File.Delete(myFile);
                            }
                        }
                    }
                    catch (Exception exceptionA)
                    {
                        //MessageBox.Show("Whoops! Something went wrong.");
                    }

                    //Go to next tab
                    tabControl3.TabPages[1].Enabled = true;
                    tabControl3.SelectedTab = tabControl3.TabPages[1];
                    tabControl1.TabPages[0].Enabled = false;

                    Cursor.Current = Cursors.Default;
                    loadingBar.Visible = false;
                    MessageBox.Show("Grading completed.");
                }
            }
        }


        /*
         * Returns an array of strings in the following order:
         * 1. The graded pdf name
         * 2. The test answers
         * 3. The key answers
         * 4. The score
         */
        string[] gradeTest(string testAnswers, string keyAnswers, string testID, string testName, string student95, string classNum)//This function tries to grade a test and insert the score into the database. It also creates a pdf of the graded test in a new folder in the desktop
        {
            string directoryGradedTests = Path.GetFullPath(Directory.GetCurrentDirectory() + "/Graded Tests/");//Directory of where we add PDFs of test grade sheets
            string pdfExtension = "_.pdf";
            string pdfNameOriginal = classNum + "_" + student95 + "_" + testName;
            string pdfName = pdfNameOriginal;
            int counter = 1;

            while(File.Exists(directoryGradedTests + pdfName + pdfExtension))
            {
                pdfName = pdfNameOriginal + "(" + (counter++).ToString() + ")";
            }
            pdfName += pdfExtension;

            //Taken from https://kb.itextpdf.com/home/it7kb/examples/creating-and-editing-pdf-2-0-documents
            PdfWriter writer = new PdfWriter(directoryGradedTests + pdfName, new WriterProperties().SetPdfVersion(PdfVersion.PDF_2_0));
            iText.Kernel.Pdf.PdfDocument pdfDocument = new iText.Kernel.Pdf.PdfDocument(writer);
            pdfDocument.SetTagged();
            Document document = new Document(pdfDocument);
            document.Add(new Paragraph(classNum + " - " + student95 + " - " + testName));

            int tempScore = 0;
            int totalPossibleScore = 0;
            //Make two arrays -- one for the key answers, and one for the test answers. Then compare the key and test answers.

            string[] answerKeyArray = keyAnswers.Split(',');
            for (int i = 0; i < answerKeyArray.Length && i < 150; i++)
            {
                if (answerKeyArray[i] != " " && answerKeyArray[i] != "," && answerKeyArray[i] != ", " && answerKeyArray[i] != " ," && answerKeyArray[i] != " , ")
                {
                    totalPossibleScore += 1;
                }
            }
            //Create a table for the answers in the pdf
            Table answerTable = new Table(3);
            Cell header1 = new Cell();
            header1.Add(new Paragraph("Question Number"));
            Cell header2 = new Cell();
            header2.Add(new Paragraph("Student's Answers"));
            Cell header3 = new Cell();
            header3.Add(new Paragraph("Correct Answers"));
            answerTable.AddHeaderCell(header1);
            answerTable.AddHeaderCell(header2);
            answerTable.AddHeaderCell(header3);
            //Now compare the answers between the test and the key.
            string[] answerTestArray = testAnswers.Split(',');
            for (int j = 0; j < answerKeyArray.Length && j < answerTestArray.Length && j < 150; j++)
            {
                if (answerKeyArray[j] != " ")
                {
                    if (answerTestArray[j] == answerKeyArray[j])
                    {
                        tempScore += 1;
                    }
                    answerTable.AddCell(new Cell().Add(new Paragraph((j + 1).ToString())));
                    answerTable.AddCell(new Cell().Add(new Paragraph(answerTestArray[j])));
                    answerTable.AddCell(new Cell().Add(new Paragraph(answerKeyArray[j])));
                }
            }
            //tempScore *= 100/totalPossibleScore;//Switch the score to percentage.

            document.Add(answerTable);
            document.Add(new Paragraph("Total Score:"));
            document.Add(new Paragraph(tempScore.ToString() + "/" + totalPossibleScore.ToString()));
            //MessageBox.Show("Success");
            document.Close();

            string[] stringReturnArray = new string[4];
            stringReturnArray[0] = directoryGradedTests + pdfName;
            stringReturnArray[1] = testAnswers;
            stringReturnArray[2] = keyAnswers;
            stringReturnArray[3] = tempScore.ToString();

            return stringReturnArray;
        }

        //https://stackoverflow.com/questions/3801275/how-to-convert-image-to-byte-array
        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();//Copy all pdfs into the selected folder
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.SelectedPath;
                foreach (object fileObj in listBox5.Items)
                {
                    try
                    {
                        string fileNameOriginal = fileObj.ToString();
                        string[] tempFileName = fileNameOriginal.Split('\\');
                        string fileNameNew = tempFileName[tempFileName.Length - 1].Replace("\\", "");
                        File.Copy(fileNameOriginal, folderName + "\\" + fileNameNew, true);
                    }
                    catch (Exception exc)
                    {

                    }
                }
            }
        }

        private void listBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox5_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listBox5.SelectedItem != null)
            {
                if (File.Exists(listBox5.SelectedItem.ToString()))
                {
                    System.Diagnostics.Process.Start(listBox5.SelectedItem.ToString());
                }
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem != null)
            {
                string selectedItem = listBox2.SelectedItem.ToString();

                listBox9.Items.Clear();
                listBox10.Items.Clear();
                label18.Text = "NA";

                tempQuery = sqlManager.SELECT_("dbo.Tests", new string[] { testsTableColumnNames[6] }, new object[] { selectedItem });
                if(tempQuery != null)
                {
                    try
                    {
                        //Create a temp image to load
                        byte[] byteArray = (byte[])tempQuery[8];
                        string tempImageFile = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".jpg";
                        File.WriteAllBytes(tempImageFile, byteArray);
                        pictureBox2.Visible = true;
                        panel3.Visible = true;
                        pictureBox2.Load(tempImageFile);
                        //Show the test answers
                        if(tempQuery[9] != null && tempQuery[10] != null && !DBNull.Value.Equals(tempQuery[9]) && !DBNull.Value.Equals(tempQuery[10]))
                        {
                            string testAnswers = (string)tempQuery[9];
                            string keyAnswers = (string)tempQuery[10];

                            string[] testAnswerArray = testAnswers.Split(',');
                            string[] keyAnswerArray = keyAnswers.Split(',');

                            foreach (string testAnswer in testAnswerArray)
                            {
                                string tempTestAnswer = testAnswer.Replace(",", "");
                                if (tempTestAnswer == "" || tempTestAnswer == " ")
                                    tempTestAnswer = "NA";
                                listBox9.Items.Add(tempTestAnswer);
                            }

                            foreach (string keyAnswer in keyAnswerArray)
                            {
                                string tempKeyAnswer = keyAnswer.Replace(",", "");
                                if (tempKeyAnswer == "" || tempKeyAnswer == " ")
                                    tempKeyAnswer = "NA";
                                listBox10.Items.Add(tempKeyAnswer);
                            }

                            //Show the score
                            if(tempQuery[3] != null)
                            {
                                label18.Text = ((int)tempQuery[3]).ToString();
                            }
                            else
                            {
                                label18.Text = "NA";
                            }

                            //Show the group box that has the scores
                            groupBox1.Visible = true;
                        }
                    }
                    catch (Exception tempExc)
                    {
                        panel3.Visible = false;
                        groupBox1.Visible = false;
                        listBox9.Items.Clear();
                        listBox10.Items.Clear();
                        pictureBox2.Visible = false;
                        MessageBox.Show("Could not open selected Test.");
                    }
                }
            }
        }

        // Convert an object to a byte array
        //https://stackoverflow.com/questions/1446547/how-to-convert-an-object-to-a-byte-array-in-c-sharp
        public static byte[] ObjectToByteArray(Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tabControl2.SelectedTab = tabPage5;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == "")
            {
                MessageBox.Show("Please enter the name of the class.");
            }
            else if (textBox6.Text == "")
            {
                MessageBox.Show("Please enter the course number.");
            }
            else
            {
                bool classAlreadyExists = false;
                int classIndexCounter = 1;
                string className = textBox5.Text;

                tempQuery = sqlManager.SELECT_("dbo.Classes", new string[] { classesTableColumnNames[1] }, new object[] { className });
                if(tempQuery != null)
                {
                    classAlreadyExists = true;
                    MessageBox.Show("A class with that name already exists.");
                }

                List<object[]> tempListQuery = sqlManager.SELECT_ALL_("dbo.Classes", new string[] { }, new object[] { });
                if(tempListQuery != null)
                {
                    classIndexCounter = tempListQuery.Count + 1;
                }

                if (classAlreadyExists == false)
                {
                    bool insertSuccessful = sqlManager.INSERT_("dbo.Classes", new string[] { classesTableColumnNames[0], classesTableColumnNames[1] }, new object[] { classIndexCounter, className });
                    if (insertSuccessful)
                    {
                        MessageBox.Show("Class created successfully.");
                    }
                    else
                    {
                        MessageBox.Show("Class could not be created.");
                    }
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            tabControl2.SelectedTab = tabPage7;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if(textBox7.Text == "")
            {
                MessageBox.Show("Please enter the user's ID number.");
            }
            else if(textBox8.Text == "")
            {
                MessageBox.Show("Please enter the user's password.");
            }
            else
            {
                bool userAlreadyExists = false;
                int userIndexCounter = 1;
                bool isAnAdmin = false;
                if (checkBox1.Checked == true)
                    isAnAdmin = true;
                string userID = textBox7.Text;
                string userPassword = textBox8.Text;

                tempQuery = sqlManager.SELECT_("dbo.Users", new string[] { usersTableColumnNames[1] }, new object[] { userID });
                if (tempQuery != null)
                {
                    userAlreadyExists = true;
                    MessageBox.Show("A user with that ID already exists.");
                }

                List<object[]> tempListQuery = sqlManager.SELECT_ALL_("dbo.Users", new string[] { }, new object[] { });
                if(tempListQuery != null)
                {
                    userIndexCounter = tempListQuery.Count + 1;
                }
                else
                {
                    MessageBox.Show("No index...");
                }

                if (userAlreadyExists == false)
                {

                    bool insertSuccessful = sqlManager.INSERT_("dbo.Users", new string[] { usersTableColumnNames[0], usersTableColumnNames[1], usersTableColumnNames[2], usersTableColumnNames[3], usersTableColumnNames[4] }, new object[] { userIndexCounter, userID, isAnAdmin, "", userPassword });
                    if (insertSuccessful)
                    {
                        MessageBox.Show("User created successfully.");
                    }
                    else
                    {
                        MessageBox.Show("There was an error creating this user.");
                    }
                }
                textBox7.Text = "";
                textBox8.Text = "";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tabControl2.SelectedTab = tabPage6;
            comboBox3.Text = "";
            comboBox4.Text = "";
            comboBox3.Items.Clear();
            comboBox4.Items.Clear();

            List<object[]> usersArray = sqlManager.SELECT_ALL_("dbo.Users", new string[] {}, new object[] {});
            if (usersArray != null)
            {
                int numRows = usersArray.Count;
                for (int i = 0; i < numRows; i++)
                {
                    comboBox3.Items.Add(usersArray[i][1].ToString());
                }
            }
            List<object[]> classesArray = sqlManager.SELECT_ALL_("dbo.Classes", new string[] { }, new object[] { });
            if (classesArray != null)
            {
                int numRows = classesArray.Count;
                for (int j = 0; j < numRows; j++)
                {
                    comboBox4.Items.Add(classesArray[j][1].ToString());
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            bool foundFirstItem = false;
            string associatedClassesString = "";
            string userIDString = comboBox3.Text;
            string classNameString = comboBox4.Text;

            tempQuery = sqlManager.SELECT_("dbo.Users", new string[] { usersTableColumnNames[1] }, new object[] { userIDString });
            if (tempQuery != null)
            {
                if(tempQuery[3] != null)
                    associatedClassesString = tempQuery[3].ToString();
                foundFirstItem = true;
            }

            if (foundFirstItem)
            {
                string theClassID = "";
                bool foundSecondItem = false;

                tempQuery = sqlManager.SELECT_("dbo.Classes", new string[] { classesTableColumnNames[1] }, new object[] { classNameString });
                if (tempQuery != null)
                {
                    theClassID = tempQuery[0].ToString();
                    foundSecondItem = true;
                }

                if (foundSecondItem)
                {
                    if (associatedClassesString == "")
                        associatedClassesString = theClassID;
                    else
                        associatedClassesString = associatedClassesString + ", " + theClassID;
                    //Now insert the class into the db
                    bool updateSuccessful = sqlManager.UPDATE_("dbo.Users", new string[] { "AssociatedClasses" }, new object[] { associatedClassesString }, new string[] { "UOID" }, new object[] { userIDString });
                    if (updateSuccessful)
                    {
                        MessageBox.Show("Successfully added " + userIDString + " to " + classNameString + ".");
                    }
                    else
                    {
                        MessageBox.Show("Could not add " + userIDString + " to " + classNameString + ".");
                    }
                    comboBox3.Text = "";
                    comboBox4.Text = "";
                }
                else
                {
                    MessageBox.Show("The class you selected doesn't exist.");
                }
            }
            else
            {
                MessageBox.Show("The user ID you selected doesn't exist.");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            int highestTestIndex = 1;
            List<object[]> tempListQuery = sqlManager.SELECT_ALL_("dbo.Tests", new string[] { }, new object[] { });
            highestTestIndex += tempListQuery.Count;

            int imageCounter = 0;
            int listBoxCounter = 0;
            foreach (var vfile in listBox5.Items)
            {
                string file = vfile.ToString();
                if (File.Exists(file))
                {
                    byte[] imageBytes = { 1 };
                    string imageFileString = listBox3.Items[imageCounter].ToString();
                    if (File.Exists(imageFileString))
                    {
                        //Convert pdf to image and save to database
                        PdfToImage.PDFConvert pdfConversion = new PDFConvert();
                        pdfConversion.OutputFormat = "jpeg";
                        pdfConversion.JPEGQuality = 100;
                        pdfConversion.ResolutionX = 300;
                        pdfConversion.ResolutionY = 300;
                        pdfConversion.FirstPageToConvert = 1;
                        pdfConversion.LastPageToConvert = 1;
                        string convertedPdf = Directory.GetCurrentDirectory() + "\\convertedpdf.jpg";
                        pdfConversion.Convert(imageFileString, convertedPdf);
                        imageBytes = File.ReadAllBytes(convertedPdf);
                    }
                    byte[] pdfBytes = File.ReadAllBytes(file);
                    string[] fileFormatter = file.Split('\\');
                    string[] fileInfoPieces = (fileFormatter[fileFormatter.Length - 1]).Split('_');
                    int classID = Int32.Parse(fileInfoPieces[0].Replace("_", ""));
                    string testName = (fileInfoPieces[1] + fileInfoPieces[2]).Replace("_", " ");

                    //Score for database is always 0 at this point. This will probably change in the future.
                    //Term for database is always Fall at this point. This will probably change in the future.
                    sqlManager.INSERT_("dbo.Tests", new string[] { "Id", "AssociatedClassID", "IsAnswerKey", "Score", "Term", "Version", "Name", "PDF", "Image", "TestAnswers", "KeyAnswers"}, new object[] { highestTestIndex++, classID, false, Int32.Parse(listBox8.Items[listBoxCounter].ToString()), "Fall", "1", testName, pdfBytes, imageBytes, listBox6.Items[listBoxCounter].ToString(), listBox7.Items[listBoxCounter].ToString() });
                }
                imageCounter += 1;
                listBoxCounter += 1;
            }
            MessageBox.Show("Files successfully saved to database.");
            button8.Enabled = false;
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                pictureBox2.Location = new Point(
                    (pictureBox2.Location.X - lastLocation.X) + e.X, (pictureBox2.Location.Y - lastLocation.Y) + e.Y);

                pictureBox2.Update();
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel3.Visible = false;
            groupBox1.Visible = false;
            listBox9.Items.Clear();
            listBox10.Items.Clear();
            panel2.Visible = false;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            //Download the pdf
            DialogResult result = folderBrowserDialog2.ShowDialog();
            if (result == DialogResult.OK)
            {
                string selectedFolder = folderBrowserDialog2.SelectedPath;

                tempQuery = sqlManager.SELECT_("dbo.Tests", new string[] { testsTableColumnNames[6] }, new object[] { listBox2.SelectedItem.ToString() });
                if(tempQuery != null)
                {
                    byte[] pdfByteArray = (byte[])tempQuery[7];
                    string fileName = Path.Combine(selectedFolder, (string)tempQuery[6]) + ".pdf";
                    File.WriteAllBytes(fileName, pdfByteArray);
                    System.Diagnostics.Process.Start(fileName);
                }
                else
                {
                    MessageBox.Show("Could not download info.");
                }
            }
        }

        // Respond to the mouse wheel (zoom in and out). http://csharphelper.com/blog/2017/08/use-mouse-wheel-scale-image-c/
        private void pictureBox2_MouseWheel(object sender, MouseEventArgs e)
        {
            if (pictureBox2.Image != null && ImageHeight != -1 && ImageWidth != -1)
            {
                // The amount by which we adjust scale per wheel click.
                const float scale_per_delta = 0.1f / 120;

                // Update the drawing based upon the mouse wheel scrolling.
                ImageScale += e.Delta * scale_per_delta;
                if (ImageScale < 0) ImageScale = 0;

                // Size the image.
                pictureBox2.Size = new Size(
                    (int)(ImageWidth * ImageScale),
                    (int)(ImageHeight * ImageScale));
            }
        }
    }
}
