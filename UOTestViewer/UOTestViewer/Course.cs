﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOTestViewer
{
    //This class holds the data for each individual Class (called a course, because class is a reserved word).
    public class Course
    {
        int ID;
        string Name;

        public Course(int id, string name)
        {
            ID = id;
            Name = name;
        }

        public int getID() { return ID; }
        public string getName() { return Name; }
    }
}
