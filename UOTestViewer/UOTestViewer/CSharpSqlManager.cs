﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data.Odbc;

namespace UOTestViewer
{
    public class CSharpSqlManager
    {
        private string connectionString;

        /// <summary>
        /// This default constructor throws an exception, because we have to pass in a connection string for this class to work.
        /// </summary>
        CSharpSqlManager()
        {
            Exception exc = new Exception("You can't create an instance of CSharpSqlManager without passing in a connection string.");
            throw exc;
        }

        /// <summary>
        /// This is the proper constructor that should be used. Takes in a connection string.
        /// </summary>
        public CSharpSqlManager(string con)
        {
            connectionString = con;
            if(connectionString == "")
            {
                Exception exc = new Exception("You can't create an instance of CSharpSqlManager with that connection (the connection may be invalid).");
                throw exc;
            }

            try //tries to connect to database before anything
            {
                var connection = new SqlConnection(connectionString);
            }
            catch (Exception e)//if fails, the class throws an exception
            {
                Exception exc = new Exception("You can't create an instance of CSharpSqlManager with that connection (the connection may be invalid).");
                throw exc;
            }
        }

        /// <summary>
        /// Creates a table of the given name with the given name-value pairs.
        /// Takes in the table name as a string. Takes in the name-value pairs as a long string
        /// [E.G. First_Name char(50),Last_Name char(50),Address char(50),City char(50),Country char(25)]
        /// Returns true if the table was successfully created. Returns false if the table already exists,
        /// or if the creation was unsuccessful for some other reason.
        /// </summary>
        public bool CREATE_TABLE(string tableName, string nameValuePairString)
        {
            //https://www.c-sharpcorner.com/article/create-a-sql-server-database-dynamically-in-C-Sharp/

            var sql = "CREATE TABLE " + tableName + "(" + nameValuePairString + ")";
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(sql, connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the table of the given name exists.
        /// </summary>
        public bool TABLE_EXISTS(string tableName)
        {
            //https://www.codeproject.com/Questions/1191245/Check-SQL-table-exist-or-not-in-Csharp
            bool exists;

            var sql = "SELECT * FROM " + tableName;

            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var cmd = new SqlCommand(sql, connection))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
                exists = true;
            }
            catch (Exception)
            {
                exists = false;
            }

            return exists;
        }

        /// <summary>
        /// Inserts a single row of data into a database.
        /// Takes in the table name as a string, the parameter names as strings, and the parameters as objects.
        /// Returns true if the insertion was successful, and false if otherwise.
        /// </summary>
        public bool INSERT_(string tableName, string[] parameters, object[] values)
        {
            if (parameters.Length != values.Length)
                return false;
            //https://stackoverflow.com/questions/12142806/how-to-insert-records-in-database-using-c-sharp-language
            //https://www.techonthenet.com/sql_server/delete.php
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    string sql = "INSERT INTO " + tableName + "(";
                    for (int i = 0; i < parameters.Length - 1; i++)
                        sql += (parameters[i] + ", ");
                    sql += parameters[parameters.Length - 1];
                    sql += ") VALUES(@";
                    for (int i = 0; i < parameters.Length - 1; i++)
                        sql += (parameters[i] + ", @");
                    sql += parameters[parameters.Length - 1];
                    sql += ")";
                    connection.Open();
                    using (var cmd = new SqlCommand(sql, connection))
                    {
                        for (int i = 0; i < parameters.Length; i++)
                            cmd.Parameters.AddWithValue("@" + parameters[i], values[i]);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch(Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Updates rows in the database based on the given parameters
        /// Takes in the table name as a string, the parameter names as strings, and the parameters as objects.
        /// Returns true if the deletion was successful, and false if otherwise.
        /// </summary>
        public bool UPDATE_(string tableName, string[] set_parameters, object[] set_values, string[] where_parameters, object[] where_values)
        {
            if (set_parameters.Length != set_values.Length || where_parameters.Length != where_values.Length)
                return false;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    string sql = "UPDATE " + tableName + " SET ";
                    for(int i = 0; i < set_parameters.Length - 1; i++)
                    {
                        sql += (set_parameters[i] + "=@" + set_parameters[i] + ", ");
                    }
                    sql += (set_parameters[set_parameters.Length - 1] + "=@" + set_parameters[set_parameters.Length - 1]);
                    if (where_parameters.Length > 0)
                    {
                        sql += " WHERE ";
                        for (int i = 0; i < where_parameters.Length - 1; i++)
                        {
                            sql += (where_parameters[i] + "=@" + where_parameters[i] + "2" + " AND");
                        }
                        sql += (set_parameters[where_parameters.Length - 1] + "=@" + where_parameters[where_parameters.Length - 1] + "2");
                    }
                    connection.Open();
                    using (var cmd = new SqlCommand(sql, connection))
                    {
                        for (int i = 0; i < set_parameters.Length; i++)
                            cmd.Parameters.AddWithValue("@" + set_parameters[i], set_values[i]);
                        for(int i = 0; i < where_parameters.Length; i++)
                            cmd.Parameters.AddWithValue("@" + where_parameters[i] + "2", where_values[i]);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Deletes a single row of data from a database.
        /// Takes in the table name as a string, the parameter names as strings, and the parameters as objects.
        /// Returns true if the deletion was successful, and false if otherwise.
        /// </summary>
        public bool DELETE_(string tableName, string[] parameters, object[] values)
        {
            if (parameters.Length != values.Length)
                return false;
            //https://stackoverflow.com/questions/9577349/delete-a-row-from-a-sql-server-table
            //https://www.techonthenet.com/sql_server/delete.php
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    string sql = "DELETE FROM " + tableName;
                    if (parameters.Length > 0)
                    {
                        sql += " WHERE ";
                        for (int i = 0; i < parameters.Length - 1; i++)
                            sql += (parameters[i] + " = " + " @" + parameters[i] + " AND ");
                        sql += parameters[parameters.Length - 1] + " = " + " @" + parameters[parameters.Length - 1];
                    }
                    connection.Open();
                    using (var cmd = new SqlCommand(sql, connection))
                    {
                        for (int i = 0; i < parameters.Length; i++)
                            cmd.Parameters.AddWithValue("@" + parameters[i], values[i]);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Retrieves the first row data that matches the given parameters from a database.
        /// Takes in the table name as a string, the parameter names as strings, and the parameters as objects.
        /// Returns the object array(a row of table data) we're searching for if it's found, and null if otherwise.
        /// </summary>
        public object[] SELECT_(string tableName, string[] parameters, object[] values)
        {
            object[] returnArray = null;
            if (parameters.Length != values.Length)
                return null;
            //https://www.techonthenet.com/sql_server/delete.php
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    string sql = "SELECT * FROM " + tableName;
                    if (parameters.Length > 0)
                    {
                        sql += " WHERE ";
                        for (int i = 0; i < parameters.Length - 1; i++)
                            sql += (parameters[i] + "=" + "@" + parameters[i] + " AND ");
                        sql += parameters[parameters.Length - 1] + "=" + "@" + parameters[parameters.Length - 1];
                    }
                    connection.Open();
                    using (var cmd = new SqlCommand(sql, connection))
                    {
                        for (int i = 0; i < parameters.Length; i++)
                            cmd.Parameters.AddWithValue("@" + parameters[i], values[i]);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                returnArray = new object[reader.FieldCount];
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    returnArray[i] = reader[i];
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("An error occured");
                return null;
            }
            return returnArray;
        }

        /// <summary>
        /// Retrieves all rows of data that matches the given parameters from a database.
        /// Takes in the table name as a string, the parameter names as strings, and the parameters as objects.
        /// Returns a list of object arrays(a row of table data) we're searching for if any are found, and null if otherwise.
        /// </summary>
        public List<object[]> SELECT_ALL_(string tableName, string[] parameters, object[] values)
        {
            if (parameters.Length != values.Length)
                return null;

            List<object[]> returnList = new List<object[]>();

            //https://www.techonthenet.com/sql_server/delete.php
            try
            {
            using (var connection = new SqlConnection(connectionString))
                {
                    string sql = "SELECT * FROM " + tableName;
                    if (parameters.Length > 0)
                    {
                        sql += " WHERE ";
                        for (int i = 0; i < parameters.Length - 1; i++)
                            sql += (parameters[i] + " = " + " @" + parameters[i] + " AND ");
                        sql += parameters[parameters.Length - 1] + " = " + " @" + parameters[parameters.Length - 1];
                    }
                    connection.Open();
                    using (var cmd = new SqlCommand(sql, connection))
                    {
                        for (int i = 0; i < parameters.Length; i++)
                            cmd.Parameters.AddWithValue("@" + parameters[i], values[i]);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                returnList.Add(new object[reader.FieldCount]);
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    returnList[returnList.Count - 1][i] = reader[i];
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            return returnList;
        }
    }
}
