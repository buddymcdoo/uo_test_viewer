﻿using Microsoft.Win32.SafeHandles;
using OmrMarkEngine.Core;
using OmrMarkEngine.Core.Processor;
using OmrMarkEngine.Output;
using OmrMarkEngine.Output.Transforms;
using OmrMarkEngine.Template;
using PdfToImage;
using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace UOTestViewer
{
    static class Program
    {
        [DllImport("kernel32.dll",
            EntryPoint = "GetStdHandle",
            SetLastError = true,
            CharSet = CharSet.Auto,
            CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr GetStdHandle(int nStdHandle);
        [DllImport("kernel32.dll",
            EntryPoint = "AllocConsole",
            SetLastError = true,
            CharSet = CharSet.Auto,
            CallingConvention = CallingConvention.StdCall)]
        private static extern int AllocConsole();
        private const int STD_OUTPUT_HANDLE = -11;
        private const int MY_CODE_PAGE = 437;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PrimaryUserForm());
            /*
             * END OF MAIN
             */
        }

        /*
        //This function analyzes the image and returns a string of the analyzed data. This is, for the most part, the be-all-end-all of our program.
        */
        public static string scanImage(string fileLocation)
        {
            string programLocation = Directory.GetCurrentDirectory();
            string tempDocumentLocation = fileLocation;
            FileInfo documentInfo = new FileInfo(tempDocumentLocation);
            string filePath = documentInfo.DirectoryName;
            string extensionType = documentInfo.Extension;
            string documentLocation = "";
            //If it's a pdf, save it as a jpg
            if (extensionType == ".pdf")
            {
                PdfToImage.PDFConvert pdfConversion = new PDFConvert();
                pdfConversion.OutputFormat = "jpeg";
                pdfConversion.JPEGQuality = 100;
                pdfConversion.ResolutionX = 300;
                pdfConversion.ResolutionY = 300;
                pdfConversion.FirstPageToConvert = 1;
                pdfConversion.LastPageToConvert = 1;
                pdfConversion.Convert(tempDocumentLocation, filePath + "\\convertedpdf.jpg");
                documentLocation = filePath + "\\convertedpdf.jpg";
            }
            else
                documentLocation = tempDocumentLocation;

            Image image = Image.FromFile(documentLocation);

            //Now, save edited image to new file location
            try
            {
                image.Save(AppDomain.CurrentDomain.BaseDirectory + "\\Image Processing\\Edited Images\\EditedImage.jpg");
            }
            catch
            {
                MessageBox.Show("Odd error: GDI+");
            }

            //Get rid of all parts that aren't deep black color                       <---------------This may possibly be implemented in the future; but it is not working right now.

            //-------------------------------------------------------------------------------

            string updatedDocumentLocation = AppDomain.CurrentDomain.BaseDirectory + "\\Image Processing\\Edited Images\\EditedImage.jpg";

            //Now the image is ready for analysis
            ScannedImage document = new ScannedImage(updatedDocumentLocation);
            document.Analyze();
            document.PrepareProcessing();
            Image tempImageForConsole = document.GetCorrectedImage();

            //Begin analyzing answers
            OmrTemplate template = OmrTemplate.Load(programLocation + "\\Image Processing\\Templates\\UO Psych Dept Test Form Template 3.xml");

            Engine engine = new Engine();
            OmrPageOutput output = engine.ApplyTemplate(template, document);

            OmrPageOutputCollection outputCollection = new OmrPageOutputCollection(output);
            outputCollection.Save(programLocation + "\\Image Processing\\Completed Jobs\\ finished.xml");

            CsvOutputTransform transformToReadable = new CsvOutputTransform();
            byte[] byteArray = transformToReadable.Transform(template, outputCollection);
            MemoryStream byteStream = new MemoryStream(byteArray);
            StreamReader byteToText = new StreamReader(byteStream);
            string unorganizedOutput = byteToText.ReadToEnd(); //An unordered string of answers
            string organizedOutput = transformToReadable.getData();//This string contains all the answers in order. THIS is what we want for output!

            return organizedOutput;
        }
    }
}
